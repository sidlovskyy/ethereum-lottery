import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { getEthPriceNow } from 'get-eth-price';
import { get } from 'https';

@Component({
  selector: 'app-eth-price',
  templateUrl: './eth-price.component.html',
  styleUrls: ['./eth-price.component.css']
})
export class EthPriceComponent implements OnInit, OnChanges {

  @Input() amount = 0;

  private rate = 0;
  private price = 0;

  constructor() { }

  async ngOnInit() {
    const allRates = await getEthPriceNow();
    const todaysRates = allRates[Object.keys(allRates)[0]];
    const ethRates = todaysRates[Object.keys(todaysRates)[0]];
    this.rate =  ethRates['USD'];
  }

  ngOnChanges(model: SimpleChanges) {
    const amountProperty = model['amount'];
    if (amountProperty) {
      this.price = this.rate * amountProperty.currentValue;
    }
  }
}
