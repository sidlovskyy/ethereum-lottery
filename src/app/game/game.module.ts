import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { RollComponent } from './roll/roll.component';
import { GameComponent } from './game.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule
  ],
  exports: [
    GameComponent
  ],
  declarations: [RollComponent, GameComponent]
})
export class GameModule { }
