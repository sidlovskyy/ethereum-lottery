import { Component, OnInit, ViewChild, NgZone } from '@angular/core';

import Web3 = require('web3');

import { Web3Service } from '../shared/web3.service';
import { ErrorService } from '../shared/error/error.service';
import { GameContractService } from '../shared/game-contract.service';

import { RollComponent } from './roll/roll.component';

type GameStatus = 'won' | 'lost' | 'wait...' | '';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  @ViewChild(RollComponent) private rollComponent: RollComponent;

  web3: Web3;

  playAmount = 0.1;
  balanceAmount = 10;
  balance: number;
  status: GameStatus = '';
  inGame = false;

  constructor(
    private ngZone: NgZone,
    private errorService: ErrorService,
    private web3Service: Web3Service,
    private gameService: GameContractService) { }

  ngOnInit() {
    this.web3 = this.web3Service.getWeb3();

    // TODO: Just for now
    this.gameService.addWarcher((err, res: Web3.SolidityEvent<any>) => this.ngZone.run(async () => {
        if (err) {
          this.errorService.logError(err);
        }

        if (res.event === 'LogWin') {
          this.rollComponent.stop(res.args._slot);
          this.status = 'won';
          await this.showBalance();
          this.inGame = false;
        }

        if (res.event === 'LogLost') {
          this.rollComponent.stop(res.args._slot);
          this.status = 'lost';
          await this.showBalance();
          this.inGame = false;
        }
    }));
  }

  async play() {
    const wei = this.toWei(this.playAmount);
    await this.gameService.play(wei);

    this.inGame = true;
    this.status = 'wait...';
    this.rollComponent.start();
  }

  async showBalance() {
    this.balance = await this.gameService.getBalance();
  }

  async increaseBalance() {
    const wei = this.toWei(this.balanceAmount);
    await this.gameService.increaseBalance(wei);
    await this.showBalance();
  }

  async withdrawGameBalance() {
    const wei = this.toWei(this.balanceAmount);
    await this.gameService.withdrawBalance(wei);
    await this.showBalance();
  }

  private toWei(ether: number | string): number {
    return Number(this.web3.toWei(ether, 'ether'));
  }
}
