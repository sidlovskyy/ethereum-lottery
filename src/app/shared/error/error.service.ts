import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class ErrorService {

  errors: EventEmitter<string> = new EventEmitter();

  constructor() { }

  logError(message: string) {
    console.log(`Error: ${message}`);
    this.errors.emit(message);
  }
}
