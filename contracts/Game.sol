pragma solidity ^0.4.11;
import "./github.com/oraclize/ethereum-api/oraclizeAPI.sol";

contract Game is usingOraclize {

    enum BetStatus {
        NoProcessed,
        InProcess,
        Failed,
        Lost,
        WinSmall,
        WinLarge,
        WinJackpot
    }

    struct Bet {
        address player;
        uint betValue;
        uint winValue;
        BetStatus status;
    }

    uint constant ORACLIZE_GAS_LIMIT = 200000 wei;
    uint constant SAFE_GAS = 2500 wei;

    uint constant NUMBER_OF_SLOTS = 3;
    uint constant NUMBER_OF_CARDS = 10;
    uint constant MIN_RANDOM_NUMBER = 0;
    uint constant MAX_RANDOM_NUMBER = 9;

    uint constant WIN_SMALL_MULTIPLIER = 2;
    uint constant WIN_LARGE_MULTIPLIER = 25;
    uint constant WIN_JACKPOT_MULTIPLIER = 100;
    uint constant JACKPOT_NUMBER = 7;

    address public owner;

    bool public isStopped;
    uint minBet = 1200000000000000 wei;
    uint maxBet = 120000000000000000 wei;
        
    mapping(bytes32 => Bet) public bets;
    mapping(address => bytes32[]) public playerBets;
    mapping(address => uint) public playerBalances;


    event LogFee(uint _fee); 
    event LogCallback(string _generated); 

    event LogFail(bytes32 indexed _key); 
    event LogLost(bytes32 indexed _key, uint[3] _slot, uint _betAmount); 
    event LogWin(bytes32 indexed _key, uint[3] _slot, uint _betAmount, uint _winAmount); 

    // OWNER - START
    
    function Game() {
        owner = msg.sender;
        start();

        // NOTE: This is for orcale testing purposes only
        OAR = OraclizeAddrResolverI(0x6f485C8BF6fc43eA212E93BBF8ce046C7f1cb475);
    }
    
    function getGameBalance() public onlyOwner constant returns(uint) {
        return this.balance;
    }
    
    function increaseGameBalance() public onlyOwner payable { }

    function withdrawGameBalance(uint _value) public onlyOwner {
        require(_value > 0);
        require(this.balance >= _value);

        owner.transfer(_value);
    }
    
    function stop() public onlyOwner {
        isStopped = true;
    }
    
    function start() public onlyOwner {
        isStopped = false;
    }

    function setMinBet(uint _value) public onlyOwner {
        require(_value > 0);
        require(_value < maxBet);
        
        minBet = _value;
    }

    function setMaxBet(uint _value) public onlyOwner {
        require(_value > 0);
        require(_value > minBet);

        maxBet = _value;
    }

    function kill() public onlyOwner {
        selfdestruct(owner);
    }

    // OWNER - END

    // PLAYER - START

    function increaseBalance() public payable onlyIfNotStopped {
        require(msg.value > 0);
        
        playerBalances[msg.sender] += msg.value;
    }

    function withdrawBalance(uint _value) public onlyPlayer {
        require(_value > 0);
        require(this.balance >= _value);
        require(playerBalances[msg.sender] >= _value);

        playerBalances[msg.sender] -= _value;
        msg.sender.transfer(_value);
    }

    function withdrawAllBalance() public onlyPlayer {
        var withdraw = playerBalances[msg.sender];
        require(this.balance >= withdraw);

        playerBalances[msg.sender] = 0;
        msg.sender.transfer(withdraw);
    }

    function getBalance() public constant returns(uint) {
        return playerBalances[msg.sender];
    }

    // PLAYER - END
    
    // PLAY - START

    function play(uint _bet) public onlyPlayer onlyIfNotStopped {
        require(_bet > 0);
        require(playerBalances[msg.sender] >= _bet);

        uint betValue = substractOraclizeFee(_bet);
        require(isBetValuePossible(betValue));
        
        bytes32 queryKey = queryForRandomGeneration();

        bets[queryKey] = Bet(msg.sender, betValue, 0, BetStatus.NoProcessed);
        playerBets[msg.sender].push(queryKey);
    }

    function substractOraclizeFee(uint _payed) private returns(uint) {
        uint oraclizeFee = oraclize_getPrice("URL", ORACLIZE_GAS_LIMIT + SAFE_GAS);

        LogFee(oraclizeFee);

        require(oraclizeFee < _payed);
        return _payed - oraclizeFee;
    }

    function isBetValuePossible(uint _betValue) private 
        constant returns(bool) {
        return (minBet <= _betValue) && (maxBet >= _betValue) && (_betValue * WIN_JACKPOT_MULTIPLIER < this.balance);
    }

    function queryForRandomGeneration() private returns(bytes32) {
        return oraclize_query(
            "URL",
           "json(https://api.random.org/json-rpc/1/invoke).result.random.data",
            // Encrypted: ' {"jsonrpc":"2.0","method":"generateIntegers","params":{"apiKey":"MY_KEY","n":3,"min":0,"max":9},"id":1}'
            "BNzxbDjIz+QTfljq0xJk4MvXvQjRJ6wP0IO0wjIhw+CehrqRmL8JIbCVl9tM9Q9uz0yyTcykL9pAGC3v9nAxR7hH7h6snvOM8F5D+yjY9Rz/LvKMTU1io7GG5vzM7DnsR9VzcbqbadDspSOgHVyd7wtmOvZDBUyFpBSsNrDjGxZ5TmoWGrr+K+NhWYfEB2H8aEGkng6Krvd9nrP2g1GP6Jw/j0PClixQuBRteSN4dAfkdEAaNF8jY3cfPEwQs7a6UgXCa1Y6cPjxiMcj0AHRUdRm/ATVNgg=",
            ORACLIZE_GAS_LIMIT + SAFE_GAS
        );
    }

    function __callback(bytes32 _key, string _generated)
        onlyOraclize
        onlyIfBetExist(_key)
        onlyIfNotProcessed(_key) {
        
        LogCallback(_generated);

        Bet storage bet = bets[_key];
        bet.status = BetStatus.InProcess;

        uint[3] memory slots = parseInt3String(_generated);

        if (isFailed(slots)) { 
            processFail(_key);
        } else if (isJackpotWin(slots)) {
            processWin(_key, slots, BetStatus.WinJackpot);          
        } else if (isLargeWin(slots)) {
            processWin(_key, slots, BetStatus.WinLarge);          
        } else if (isSmallWin(slots)) {
            processWin(_key, slots, BetStatus.WinSmall);            
        } else { 
            processLoose(_key, slots);            
        }
    }

    function parseInt3String(string _text) private constant returns (uint[3] result) {
        // NOTE: Response in format [x, x, x] which we are going to parse
        // indexes:                 012345678 - total: 9
        bytes memory textBytes = bytes(_text);
        require(textBytes.length == 9);

        result[0] = uint(textBytes[1]) - 48;
        result[1] = uint(textBytes[4]) - 48;
        result[2] = uint(textBytes[7]) - 48;
    }

    function isFailed(uint[3] memory _result) private constant returns(bool) {
        return (
            _result[0] < MIN_RANDOM_NUMBER || _result[0] > MAX_RANDOM_NUMBER || 
            _result[1] < MIN_RANDOM_NUMBER || _result[1] > MAX_RANDOM_NUMBER ||
            _result[2] < MIN_RANDOM_NUMBER || _result[2] > MAX_RANDOM_NUMBER 
        );
    }

    function isSmallWin(uint[3] memory _result) private constant returns(bool) {
        return (
            (_result[0] == _result[1]) || (_result[1] == _result[2])
        );
    }
    
    function isLargeWin(uint[3] memory _result) private constant returns(bool) {
        return (
            (_result[0] == _result[1]) && (_result[1] == _result[2])
        );
    }

    function isJackpotWin(uint[3] memory _result) private constant returns(bool) {
        return (
            (_result[0] == JACKPOT_NUMBER) && (_result[1] == JACKPOT_NUMBER) && (_result[2] == JACKPOT_NUMBER) 
        );
    }

    function processFail(bytes32 _key) private {
        Bet storage bet = bets[_key];
        bet.status = BetStatus.Failed;

        LogFail(_key);
    }

    function processWin(bytes32 _key, uint[3] memory _slots, BetStatus _status) private {
        require((_status == BetStatus.WinJackpot) || (_status ==  BetStatus.WinLarge) || (_status ==  BetStatus.WinSmall));

        uint multiplier = getWinMultiplier(_status);

        Bet storage bet = bets[_key];
        bet.status = _status;
        bet.winValue = bet.betValue * (multiplier - 1);

        playerBalances[bet.player] += bet.winValue;

        LogWin(_key, _slots, bet.betValue, bet.winValue);
    }

    function getWinMultiplier(BetStatus _status) private constant returns(uint) {
        if(_status == BetStatus.WinJackpot) {
            return WIN_JACKPOT_MULTIPLIER;
        }

        if(_status == BetStatus.WinLarge) {
            return WIN_LARGE_MULTIPLIER;
        }

        if(_status == BetStatus.WinSmall) {
            return WIN_SMALL_MULTIPLIER;
        }

        return 1;
    }

    function processLoose(bytes32 _key, uint[3] memory _slots) private {
        Bet storage bet = bets[_key];
        bet.status = BetStatus.Lost;

        playerBalances[bet.player] -= bet.betValue;

        LogLost(_key, _slots, bet.betValue);
    }

    // PLAY - END

    // MODIFIERS - START

    modifier onlyIfNotStopped {
        require(!isStopped);
        _;
    }

    modifier onlyOraclize {
        require(msg.sender == oraclize_cbAddress());
        _;
    }

    modifier onlyIfNotProcessed(bytes32 _key) {
        require(bets[_key].status == BetStatus.NoProcessed);
        _;
    }

    modifier onlyIfBetExist(bytes32 _key) {
        require(bets[_key].player != address(0x0));
        _;
    }
    
    modifier onlyOwner() {
        require(owner == msg.sender);
        _;
    }

    modifier onlyPlayer() {
        require(playerBalances[msg.sender] > 0);
        _;
    }

    // MODIFIERS - END
}