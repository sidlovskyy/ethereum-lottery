import { Component, OnInit  } from '@angular/core';

import Web3 = require('web3');

import { Web3Service } from '../shared/web3.service';
import { ErrorService } from '../shared/error/error.service';
import { GameContractService } from '../shared/game-contract.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(
    private errorService: ErrorService,
    private web3Service: Web3Service,
    private gameService: GameContractService) { }

  owner: string;

  balance: number;
  amount = 50;

  accounts: string[];

  web3: Web3;

  async ngOnInit() {

    // TODO: Move it out of here to bootstrap
    this.web3 = this.web3Service.getWeb3();

    this.accounts = await this.getAccounts();
    this.web3.eth.defaultAccount = this.accounts[0];
    console.log(`Account: ${this.accounts[0]}`);

    await this.getOwner();
    await this.showGameBalance();
  }

  async getOwner() {
    this.owner = await this.gameService.getOwner();
  }

  async showGameBalance() {
    this.balance = await this.gameService.getGameBalance();
  }

  async increaseGameBalance() {
    const wei = this.toWei(this.amount);
    await this.gameService.increaseGameBalance(wei);
    await this.showGameBalance();
  }

  async withdrawGameBalance() {
    const wei = this.toWei(this.amount);
    await this.gameService.withdrawGameBalance(wei);
    await this.showGameBalance();
  }

  getAccounts(): Promise<string[]> {
    const promise = new Promise<string[]>((resolve, reject) => {
      this.web3.eth.getAccounts((err, accounts) => {
        if (err) {
          this.errorService.logError('Cannot load ethereum accounts');
          reject();
        } else {
          resolve(accounts);
        }
      });
    });

    return promise;
  }

  private toWei(ether: number | string): number {
    return Number(this.web3.toWei(ether, 'ether'));
  }
}
