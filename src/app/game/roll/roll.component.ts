import { Component, OnInit } from '@angular/core';
import { ErrorService } from '../../shared/error/error.service';

@Component({
  selector: 'app-roll',
  templateUrl: './roll.component.html',
  styleUrls: ['./roll.component.css']
})
export class RollComponent implements OnInit {

  slots: number[] = [0, 0, 0];
  isStopped = true;
  interval: NodeJS.Timer;

  constructor(private errorService: ErrorService) { }

  ngOnInit() { }

  start(): void {
    this.isStopped = false;
    this.interval = setInterval(() => {
        for (let i = 0; i < this.slots.length; i++) {
          this.slots[i] = this.random();
        }
    }, 200);
  }

  stop(result: number[]): void {
    this.isStopped = true;

    if (!result && result.length !== 3) {
      this.errorService.logError('Cannot stop slot correctly');
      return;
    }

    if (this.interval) {
      clearInterval(this.interval);
    }
    this.slots = result;
  }

  private random() {
      return Math.floor(Math.random() * 10);
  }
}
