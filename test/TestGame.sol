pragma solidity ^0.4.11;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Game.sol";

contract TestGame {
  function testInitialBalanceUsingDeployedContract() {
    Game game = Game(DeployedAddresses.Game());

    uint expected = 10000;

    Assert.equal(game.getBalance(), expected, "Owner should have 10000 MetaCoin initially");
  }
}