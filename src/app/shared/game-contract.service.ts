import { Injectable } from '@angular/core';

import Web3 = require('web3');

import { ErrorService } from './error/error.service';
import { Web3Service } from './web3.service';

import * as data from '../../../build/contracts/Game.json';

type Callback = (error: any, result: any) => void;
type Executor = (callbak: Callback) => void;
type WeiExecutor = (value: { value: number}, callbak: Callback) => void;
type NumberExecutor = (value: number, { gas: number }, callbak: Callback) => void;

@Injectable()
export class GameContractService {

  contractAddress: string;
  contractInstance: any;

  web3: Web3;

  watchers: Callback[] = [];
  filter: Web3.FilterResult;

  constructor(private errorService: ErrorService, private web3Service: Web3Service) {
    this.web3 = this.web3Service.getWeb3();

    // NOTE: We take the latest one address available
    const networks = data.networks;
    for (const network in networks) {
      if (networks.hasOwnProperty(network)) {
        this.contractAddress = networks[network].address;
      }
    }
    console.log(`Contract address: ${this.contractAddress}`);

    const abi: Web3.AbiDefinition[] = data.abi;
    const contract = this.web3.eth.contract(abi);

    this.contractInstance = contract.at(this.contractAddress);

    this.addWarcher((e, r) => {
        if (e) {
          console.log('Event Error: ' + e);
        } else {
          console.log('Event Result: ' + JSON.stringify(r));
        }
      }
    );

    this.setupWatcher();
  }

  // EVENTS - START

  addWarcher(watcher: Callback) {
    if (!watcher) {
      return;
    }

    this.watchers.push(watcher);
  }

  private setupWatcher() {
    this.contractInstance.allEvents((error, result) => {
      this.watchers.forEach(watcher => watcher(error, result));
    });
  }

  // EVENTS - END

  // OWNER - START

  getOwner(): Promise<string> {
    return this.toPromise(this.contractInstance.owner, 'Cannot get contact owner');
  }

  getGameBalance(): Promise<number> {
    return this.toPromise(
      this.contractInstance.getGameBalance,
      'Cannot get game balance',
      (response) => Number(this.web3.fromWei(response.valueOf(), 'ether'))
    );
  }

  increaseGameBalance(wei: number): Promise<string> {
    return this.toPayPromise(wei, this.contractInstance.increaseGameBalance, 'Cannot increase game balance');
  }

  withdrawGameBalance(wei: number): Promise<string> {
    return this.toNumberAcceptPromise(wei, this.contractInstance.withdrawGameBalance, 'Cannot increase withdraw balance');
  }

  isStopped(): Promise<boolean> {
    return this.toPromise(
      this.contractInstance.isStopped,
      'Cannot get contract state',
      (response) => !!response.valueOf()
    );
  }

  stop(): Promise<void> {
    return this.toPromise(this.contractInstance.stop, 'Cannot stop contract');
  }

  start(): Promise<void> {
    return this.toPromise(this.contractInstance.start, 'Cannot start contract');
  }

  setMinBet(wei: number): Promise<void> {
    return this.toNumberAcceptPromise(wei, this.contractInstance.setMinBet, 'Cannot set min bet');
  }

  setMaxBet(wei: number): Promise<void> {
    return this.toNumberAcceptPromise(wei, this.contractInstance.setMaxBet, 'Cannot set max bet');
  }

  // OWNER - END

  // PLAYER - START

  getBalance(): Promise<number> {
    return this.toPromise(
      this.contractInstance.getBalance,
      'Cannot get balance',
      (response) => Number(this.web3.fromWei(response.valueOf(), 'ether'))
    );
  }

  increaseBalance(wei: number): Promise<string> {
    return this.toPayPromise(wei, this.contractInstance.increaseBalance, 'Cannot increase balance');
  }

  withdrawBalance(wei: number): Promise<string> {
    return this.toNumberAcceptPromise(wei, this.contractInstance.withdrawBalance, 'Cannot withdraw balance');
  }

  withdrawAllBalance(): Promise<string> {
    return this.toPromise(this.contractInstance.withdrawAllBalance, 'Cannot withdraw all balance');
  }

  play(wei: number): Promise<void> {
    return this.toNumberAcceptPromise(wei, this.contractInstance.play, 'Cannot start game');
  }

  // PLAYER - END

  toPromise<T>(executor: Executor, message: string = '', mapper: (any) => T = null): Promise<T> {
    const promise = new Promise<T>((resolve, reject) => {
      executor((error, result) => {
        if (error) {
          this.errorService.logError(message);
          reject(error);
        } else {
          console.log(`Call result: ${result}`);
          const mappedResult = mapper ? mapper(result) : result;
          resolve(mappedResult);
        }
      });
    });

    return promise;
  }

  toPayPromise<T>(wei: number, executor: WeiExecutor, message: string = '', mapper: (any) => T = null): Promise<T> {
    const promise = new Promise<T>((resolve, reject) => {
      executor({value: wei}, (error, result) => {
        if (error) {
          this.errorService.logError(message);
          reject(error);
        } else {
          console.log(`Call result: ${result}`);
          const mappedResult = mapper ? mapper(result) : result;
          resolve(mappedResult);
        }
      });
    });

    return promise;
  }

  toNumberAcceptPromise<T>(value: number, executor: NumberExecutor, message: string = '', mapper: (any) => T = null): Promise<T> {
    const promise = new Promise<T>((resolve, reject) => {
      executor(value, {gas: 210000}, (error, result) => {
        if (error) {
          this.errorService.logError(message);
          reject(error);
        } else {
          console.log(`Call result: ${result}`);
          const mappedResult = mapper ? mapper(result) : result;
          resolve(mappedResult);
        }
      });
    });

    return promise;
  }
}
