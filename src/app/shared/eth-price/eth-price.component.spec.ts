import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EthPriceComponent } from './eth-price.component';

describe('EthPriceComponent', () => {
  let component: EthPriceComponent;
  let fixture: ComponentFixture<EthPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EthPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EthPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
