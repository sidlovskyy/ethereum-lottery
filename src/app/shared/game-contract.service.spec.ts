import { TestBed, inject } from '@angular/core/testing';

import { GameContractService } from './game-contract.service';

describe('GameContractService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameContractService]
    });
  });

  it('should be created', inject([GameContractService], (service: GameContractService) => {
    expect(service).toBeTruthy();
  }));
});
