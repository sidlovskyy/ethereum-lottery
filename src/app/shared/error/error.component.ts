import { Component, OnInit } from '@angular/core';

import { ErrorService } from './error.service';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  message: string = null;

  constructor(private errorService: ErrorService) { }

  ngOnInit() {
    this.errorService.errors.subscribe(message => {
      this.message = message;
    });
  }

  clear() {
    this.message = null;
  }

}
