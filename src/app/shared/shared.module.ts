import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Web3Service } from './web3.service';
import { ErrorComponent } from './error/error.component';
import { ErrorService } from './error/error.service';
import { GameContractService } from './game-contract.service';
import { EthPriceComponent } from './eth-price/eth-price.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    ErrorComponent,
    EthPriceComponent
  ],
  declarations: [ErrorComponent, EthPriceComponent],
  providers: [Web3Service, ErrorService, GameContractService]
})
export class SharedModule { }
